const { menu0 } = require("../menu/menu0");
const { db } = require("../models/banco");

function execute(user, msg, contato) {

    // Obtem a hora atual do PC para definir se vai ser Bom dia, tarde ou noite.
    stamp = new Date();
    hours = stamp.getHours();
    if (hours >= 18 && hours < 24) {
        time = "Boa noite"
    } else if (hours >= 12 && hours < 18) {
        time = "Boa tarde"
    } else if (hours >= 0 && hours < 12) {
        time = "Bom dia"
    }


    let menu = " ROUPAS: \n\n";

    Object.keys(menu0).forEach((value) => {
        let element = menu0[value];
        menu += `${value} - ${element.description}        R$ ${element.price} \n`;
    });

    db[user].stage = 1;

    return [
        menu,
        `${time} ${contato} Sou uma assistente virtual e estarei fazendo seu atendimento. \n
        PorFavor escolha o produto que deseja e  informe o codigo, por exemplo (1- Vestido, digite o n°1 e aperte enter) \n
        Caso deseje comprar mais de 1, porfavor mande um codigo de cada vez`,
    ];
}

exports.execute = execute;